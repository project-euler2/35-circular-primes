import math
import time
start_time = time.time()

def is_prime(n):
	if n <= 1:
		return False
	if n == 2:
		return True
	if n > 2 and n % 2 == 0:
		return False
	max_div = math.floor(math.sqrt(n))
	for i in range(3, 1 + max_div, 2):
		if n % i == 0:
			return False
	return True

def Prime_Number_In_Range(n):
	prime = [True for i in range(n+1)]
	prime_values = []
	p = 2
	while(p * p <= n):
		if (prime[p] == True):# If prime[p] is not changed, then it is a prime
			for i in range(p * p, n + 1, p): # Update all multiples of p
				prime[i] = False
		p += 1
	for j in range(2,n):
		if prime[j]:
			prime_values.append(j)
	return prime_values

def count_circular_prime(max):
    primes_in_range = Prime_Number_In_Range(max)
    circular_primes = []
    for prime in primes_in_range:
        rotation = []
        for j in range(len(str(prime))):
            new_rotation = ''
            new_rotation = str(prime)[j:] + str(prime)[0:j]
            rotation.append(int(new_rotation))
        working_number = True
        for number in rotation:
            working_number = working_number and is_prime(number)
        if working_number == True:
            circular_primes.append(prime)
    return len(circular_primes)

print(count_circular_prime(1000000))
print(f"--- {(time.time() - start_time):.10f} seconds ---" )